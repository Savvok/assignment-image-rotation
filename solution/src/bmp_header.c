#include "status_errors.h"



#include "file_handler.h"

#pragma pack(push, 1)
struct bmp_header
{
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};
#pragma pack(pop)


extern inline enum read_status status_header(const struct bmp_header* header) {
    if (header->bfType != 0x4d42) {
        return READ_INVALID_SIGNATURE;
    }
    if (header->biBitCount != 24) {
        return READ_INVALID_BITS;
    }
    return READ_OK;
} // проверки для заголовка

static inline uint8_t padding(struct bmp_header* header)
{
    return (*header).biWidth % 4;
}

enum read_status from_bmp(FILE* in, struct image* img) {

    struct bmp_header header;
    if (fread(&header, sizeof(struct bmp_header), 1, in) != 1) return READ_INVALID_HEADER; // читаем заголовок одновременно проверяя его
    if (status_header(&header) != READ_OK) return status_header(&header);
    *img = create_image(header.biWidth, header.biHeight); // выделяем память для массива пикселей
    for (size_t i = 0; i < (*img).height; i++) {
        if (!fread(&((*img).data[i * (*img).width]), sizeof(struct pixel), (header).biWidth, in)) return READ_ERROR;
        if (fseek(in, padding(&header), SEEK_CUR)) return READ_ERROR; // решаем проблему с padding байтами 
    }


    return READ_OK;
}

enum write_status to_bmp(FILE* out, struct image const* img) {

    struct bmp_header header = {
           .bfType = 0x4D42,
           .bfileSize = (sizeof(struct bmp_header) + (*img).height * (*img).width * sizeof(struct pixel)
                        + (*img).height * (((*img).width) % 4)),
           .bfReserved = 0,
           .bOffBits = sizeof(struct bmp_header),
           .biSize = 40,
           .biWidth = (*img).width,
           .biHeight = (*img).height,
           .biPlanes = 1,
           .biBitCount = 24,
           .biCompression = 0,
           .biSizeImage = (*img).height * (*img).width * sizeof(struct pixel) + ((*img).width % 4) * (*img).height,
           .biXPelsPerMeter = 0,
           .biYPelsPerMeter = 0,
           .biClrUsed = 0,
           .biClrImportant = 0
    }; // задаем параметры выходному bmp файлу 

    if (fwrite(&header, sizeof(struct  bmp_header), 1, out) != 1) { return WRITE_ERROR; }; //записываем заголовок одновременно проверяя его

    const uint32_t z = 0;

    for (size_t i = 0; i < (*img).height; i++) {
        if (fwrite(&((*img).data[i * (*img).width]), sizeof(struct pixel), (*img).width, out) != (header).biWidth) return WRITE_ERROR;
        if (fwrite(&z, 1, padding(&header), out) != padding(&header)) return WRITE_ERROR; // заполняем padding байты нулями
    }
    return WRITE_OK;
}
enum open_status open_file(char const* name, struct image* image) {
    FILE* in;
    enum open_status input = open_io(&in, name, "rb");
    if (input != OPEN_OK) return OPEN_ERROR;
    enum read_status readstatus = from_bmp(in, image);
    if (readstatus != READ_OK) return OPEN_ERROR;
    return OPEN_OK;
}

enum close_status close_file(char const* name, struct image* image) {
    FILE* out;
    enum open_status output = open_io(&out, name, "wb");
    if (output != OPEN_OK) return CLOSE_ERROR;
    enum write_status writestatus = to_bmp(out, image);
    if (writestatus != WRITE_OK) return CLOSE_ERROR;
    enum close_status closestatus = close_io(&out);
    if (closestatus != CLOSE_OK) return CLOSE_ERROR;
    return CLOSE_OK;
}



