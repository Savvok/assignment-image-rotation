
#include "image.h"


struct image rotate(struct image const source) {

	struct image new = create_image(source.height, source.width);
	for (uint64_t x = 0; x < source.width; x++) {
		for (uint64_t y = 0; y < source.height; y++) {
			size_t arr_index = index(x, y, source.height);
			size_t new_index = index(source.height - y - 1, x, source.width);
			new.data[arr_index] = source.data[new_index];
		}

	}
	return new;

}

