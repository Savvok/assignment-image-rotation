
#include "image.h"

struct image create_image(const uint64_t width, const uint64_t height) {
	struct pixel* arr = malloc(sizeof(struct pixel) * width * height);
	struct image new = { .width = width, .height = height, .data = arr };
	return new;
}
void delete_image(const struct image* delete) {
	free((*delete).data);
}
