#include "file_handler.h"

enum open_status open_io(FILE** in, char const* name, char const* mods) {
    if (!name) return OPEN_ERROR;
    *in = fopen(name, mods);
    if (!*in) return OPEN_ERROR;

    return OPEN_OK;
}

enum close_status close_io(FILE** out) {
    if (fclose(*out) == EOF) return CLOSE_ERROR;
    return CLOSE_OK;
}
