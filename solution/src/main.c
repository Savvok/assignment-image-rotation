#include "image.h"
#include "bmp_header.h"
#include "image_rotate.h"
#include "utils.h"

int main(int argc, char** argv) {
    if (argc != 3) {
        error_msg("Inappropriate arguments"); // проверяем аргументы
        exit(1);
    }
	struct image image; 
	enum open_status open = open_file(argv[1], &image); //открываем и проверяем поток

    if (open != OPEN_OK) {
        delete_image(&image);
        error_msg("File read error");
        exit(2);
    }
	struct image rotated = rotate(image); // поворачиваем картинку

    delete_image(&image); //освобождаем память

    enum close_status close = close_file(argv[2], &rotated); //закрываем и проверяем поток
    if (close != CLOSE_OK) {
        delete_image(&rotated);
        error_msg("Error writing to file");
        exit(3);
    }


    delete_image(&rotated);

    return 0;
}
