#ifndef F
#define F
#include "image.h"
#include "status_errors.h"
#include <stdio.h>

enum open_status open_io(FILE** in, char const* name, char const* mods);
enum close_status close_io(FILE** out);


#endif 

