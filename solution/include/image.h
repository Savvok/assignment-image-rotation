#ifndef IMAGE
#define IMAGE
#include<malloc.h>
#include <stdint.h>


struct __attribute__((packed)) pixel { uint8_t b, g, r; };
struct image {
	uint64_t width;
	uint64_t height;
	struct pixel* data;
};

struct image create_image(uint64_t width, uint64_t height);
void delete_image(const struct image* delete);
static inline size_t index(const uint64_t xw, const uint64_t yh, const uint64_t height) {
	return yh + xw * height;
}

#endif 

