#ifndef HEADER
#define HEADER
#include  <stdint.h>
#include <stdio.h>
#include "status_errors.h"


enum read_status from_bmp(FILE *in, struct image *img);

enum write_status to_bmp(FILE *out, struct image const *img);

enum open_status open_file(char const* name, struct image* image);
enum close_status close_file(char const* name, struct image* image);

#endif 
